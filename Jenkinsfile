def slackChannel = "#demoapp_alerts"
def gitRepoLink = "https://ram_kannan@bitbucket.org/ram_kannan/maven-hello-world.git"

node () {
    deleteDir()
	try {
	    stage ("scm checkout") {
	        checkoutGit('master', "maven-hello-world", "${gitRepoLink}")
	    }

	    stage ("mvn build") {
	    	withMaven(maven: 'maven3') {
	        	sh "cd maven-hello-world ; mvn clean package"
	        }
	    }
	    
	    stage ("Run Java") {
	        sh "java -cp target/hello.jar com.example.hello.Hello"
	    }    

	   	stage("Docker Deploy") {
	      	withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: 'dockerCloud', usernameVariable: 'userId', passwordVariable: 'passwd']]) {
	            dir("DemoJavaApp") {
		    		sh """
				    	docker ps -a | awk '{ print \$1,\$2 }' | grep demojavaapp | awk '{print \$1 }' | xargs -I {} docker rm {} --force
			            docker ps -a
					"""
        			sh """
        			    docker build . -t ramkannan91/mavenapp -f Dockerfile
				    	docker login -u ${userId} -p '${passwd}' docker.io
				    	docker push ramkannan91/mavenapp:latest
    				"""
	            }
	        }
	    }

	    stage("Post Build") {
	    	currentBuild.displayName = "DemoApp-${env.BUILD_NUMBER}" 
	    	slackSuccess(slackChannel)
	    }
    } catch (err) {
        println("================ ERROR: ${err}")
		currentBuild.displayName = "DemoApp-${env.BUILD_NUMBER}" 
    	slackFailure(slackChannel)
        currentBuild.result = "FAILURE"
        error()
    }
}

def checkoutGit(branchName, targetDir, repoURL) {
    checkout([$class: 'GitSCM',
      branches: [[name: branchName]],
      doGenerateSubmoduleConfigurations: false,
      extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: targetDir]],
      submoduleCfg: [],
      userRemoteConfigs: [[credentialsId: 'git-stash-commoncloud-jenkins', url: repoURL]]
    ])
}

def slackSuccess(slackChannel) {
    slackSend (
        channel: slackChannel,
        color: "#008000",
        message: ":blush: *SUCCESS*\n_Deployment_ Completed for *${currentBuild.displayName}*.\nBuild URL - ${env.BUILD_URL}.\nName - Ram Kannan S")
}

def slackFailure(slackChannel) {
    slackSend (
        channel: slackChannel,
        color: "#FF0000",
        message: ":dizzy_face: *FAILURE*\n_Deployment_ Failed for *${currentBuild.displayName}*.\nBuild URL - ${env.BUILD_URL}.\nName - Ram Kannan S")
}